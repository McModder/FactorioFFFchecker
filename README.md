# Factorio Friday FactsChecker  #
# We moved to [Gitlab.ely.by](https://gitlab.ely.by/mcmodder/oc-progs) #
## Установка ##
* Установите Luvit. Для этого посетите [luvit.io](https://luvit.io) и следуйте инструкциям по установке.
* Установите библиотеку Discordia, выполнив `lit install SinisterRectus/discordia`
* Запустите бота командой `luvit bot.lua`

## Настройка ##
Измените в `bot.lua` строку (`local token = ""`) и вставьте туда токен бота

## TODO ##
Добавить в бота команды `add`, `remove`, `list` для управления списком серверов, на которые рассылаются уведомления о новом Factorio Friday Facts

# EN: #

## Installing ##
* Install Luvit. Check instructions on [luvit.io](https://luvit.io).
* Install Discordia: `lit install SinisterRectus/discordia`
* Run bot: `luvit bot.lua`

## Configuring ##
Edit string `local token = ""` in `bot.lua`

## TODO ##
Add commands `add`, `remove`, `list` to in-bot managing server mailibg list about new Factorio Friday Facts released
