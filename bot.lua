-- config
local token = ""
-------------------------
-- libs
local client = require("discordia").Client:new()
local http = require('coro-http')
local timer = require("timer")
local colorize = require('pretty-print').colorize
local path = require("path")
require("lib/table2")
-------------------------
-- functions
local function date( time )
  return os.date("[%d.%m.%y][%X]", time)
end

local function writeLog( text, logType )
  logType = string.upper(logType or "INFO")
  local logColors = { INFO = "string", LOG = "string", WARN = "highlight", DEBUG= "highlight", ERR = "err", ERROR= "err", FAIL = "failure", FAILURE = "failure"}
  print(colorize(logColors[logType] or "string", "'"..date().."["..logType.."] "..text.."'"))
  local logfile = io.open("bot.log", "a")
  if logfile then
    logfile:write(date().."["..logType.."] "..text.."\n")
    logfile:close()
  else
    print(colorize('failure', "'"..date().."[ERR] Can't open log file!'"))
  end
end

local function internetRequest( requestType, url, callback, headers, ... )
  local head, body = http.request(requestType, url, headers)
  if head.code == 200 then
    p(url..": success")
  else
    p(url.." returned code "..head.code)
  end
  callback(head, body, ...)
end

local function checkServer( server, channel )
  local Tserver = client:getServerById(server)
  if Tserver and not channel then
    return true, Tserver
  elseif Tserver and channel then
    Tchannel = Tserver:getChannelById(channel)
    if Tchannel then return true, Tchannel end
  end
  return false
end
-------------------------
-- load last friday date and last FFF number
local lastFriday, lastFFF
local file = io.open("tLastFriday.txt", "r")
if file then
  lastFriday, lastFFF = file:read("*a"):match("^([^%^]+)%^%^%^([%d]+)$")
  file:close()
end
lastFFF = lastFFF or 158
-- load servers list to send info
local servers = table.fromFile("servers.txt")
servers = servers or {}
if table.size(servers, true) == 0 then servers = {["156999544758861824"]="166489681821827072"} end
table.toFile("servers.txt", servers, true, 2, false, 2, false)
writeLog("Loaded "..table.size(servers, true).." servers")
-------------------------
-- check new FFF
local function checkFFF(tLastFriday, tLastFFF)
  if os.date("%A") == "Friday" and tLastFriday ~= os.date("%x") then
    coroutine.wrap(internetRequest)("GET", "https://www.factorio.com/blog/post/fff-"..(tLastFFF+1), function( tHead, tBody )
      if tHead.code == 200 then
        writeLog("New FFF "..(tLastFFF+1).."!")
        tLastFFF = tLastFFF+1
        local file = io.open("tLastFriday.txt", "w")
        file:write(os.date("%x").."^^^"..tLastFFF)
        file:close()
        tLastFriday = os.date("%x")
        for server, channel in pairs(servers) do
          local success, tChannel = checkServer(server, channel)
          p(server, channel, success)
          if success==true then
            coroutine.wrap(tChannel.createMessage)(tChannel, "@everyone\n**Вышли новые Factorio Friday Facts!**\nhttps://www.factorio.com/blog/post/fff-"..tLastFFF)
          else
            writeLog("Server "..server..", channel "..channel..", ERROR: NOT FOUND", "WARN")
          end
        end
        timer.setTimeout(518400000, checkFFF, tLastFriday, tLastFFF)
        writeLog("Setting timer to 518400000 ms")
      else
        writeLog("FFF "..(tLastFFF+1)..", got 404")
        timer.setTimeout(900000, checkFFF, tLastFriday, tLastFFF)
        writeLog("Setting timer to 900000 ms")
      end
    end)
  else
    writeLog("Still not Friday...")
    timer.setTimeout(3600000, checkFFF, tLastFriday, tLastFFF)
    writeLog("Setting timer to 3600000 ms")
  end
end

client:on('resume', function()
  writeLog("Resumed as "..client.user.username, "INFO")
end)

client:on('disconnect', function(expected)
  writeLog(expected == true and "Expected disconnect" or "UNEXPECTED DISCONNECT!", "WARN")
end)

client:on('ready', function()
  writeLog("Logged in as "..client.user.username, "INFO")
  checkFFF(lastFriday, lastFFF)
end)

client:run(token)